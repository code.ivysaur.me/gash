# gash

![](https://img.shields.io/badge/written%20in-golang-blue)

A POSIX shell.

Uses goroutines to implement job backgrounding.

The tokenizer, parser, execution and piping all work, but as of the current release (0.1) there are many bugs and missing features before you could `chsh` to this.

Tags: PL

## Changelog

2016-08-14: r010
- Initial release
- [⬇️ gash-r010.7z](dist-archive/gash-r010.7z) *(4.35 KiB)*

